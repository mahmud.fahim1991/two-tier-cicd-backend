#!/bin/bash

kill -9 $(lsof -t -i:5000)

cd /home/cicd/two-tier-cicd-backend
git pull --rebase
pip3 install -r requirements.txt
export PATH="$PATH:/home/cicd/.local/bin"
flask run --host 0.0.0.0 &
exit 

